<!--

    Copyright 2013-2016 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">project</ac:parameter>
        <ac:parameter ac:name="render-as">definition-list</ac:parameter>
        <ac:parameter ac:name="extract-short-desc">true</ac:parameter>
        <ac:parameter ac:name="override">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                    <ac:parameter ac:name="attribute">description</ac:parameter>
                  </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.doctype"/></th>
                  <td class="confluenceTd">project</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:var at:name="name" /><at:var at:name="projectdoc.doctype.common.name" /></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.tags.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.spaceTags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                    <ac:parameter ac:name="attribute">labels</ac:parameter></ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">documentation-json-uri</th>
                  <td class="confluenceTd">https://www.smartics.eu/confluence/download/attachments/12156954/docmap.json?api=v2</td>
                  <td class="confluenceTd">hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>

  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="livesearch">
              <ac:parameter ac:name="placeholder"><at:i18n at:key="projectdoc.home.label.search"/></ac:parameter>
              <ac:parameter ac:name="spaceKey"><at:var at:name="spaceKeyElement" at:rawxhtml="true"/></ac:parameter>
              <ac:parameter ac:name="size">large</ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="three_with_sidebars">
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="titleBGColor">#FFF380</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.label.featured-pages"/></ac:parameter>
        <ac:parameter ac:name="borderColor">#EAC117</ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="contentbylabel">
              <ac:parameter ac:name="spaces"><at:var at:name="spaceKeyElement" at:rawxhtml="true"/></ac:parameter>
              <ac:parameter ac:name="showLabels">false</ac:parameter>
              <ac:parameter ac:name="sort">title</ac:parameter>
              <ac:parameter ac:name="labels">featured</ac:parameter>
              <ac:parameter ac:name="showSpace">false</ac:parameter>
              <ac:parameter ac:name="type">page</ac:parameter>
            </ac:structured-macro>
          </p>
          <ac:structured-macro ac:name="projectdoc-hide-from-anonymous-user-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <div style="text-align: center;">
                <hr/>
                <p><sup><strong><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page"><at:i18n at:key="projectdoc.content.doctools.content-management-dashboard.name"/></ac:parameter></ac:structured-macro></strong></sup></p>
              </div>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">documentation-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.documentation"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.tours.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.faqs.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.topics.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.glossary-items.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">library-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.resources"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.resources.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.quotes.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">team-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.references.section.hr"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#FFA62F</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.roles.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.stakeholders.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.persons.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.organizations.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="titleBGColor">white</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.label.recently-updated"/></ac:parameter>
        <ac:parameter ac:name="borderColor">#CCCCCC</ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="recently-updated">
              <ac:parameter ac:name="max">10</ac:parameter>
              <ac:parameter ac:name="hideHeading">true</ac:parameter>
              <ac:parameter ac:name="theme">concise</ac:parameter>
              <ac:parameter ac:name="types">page, comment, blogpost, spacedesc</ac:parameter>
            </ac:structured-macro> </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
    <ac:layout-cell>
      <p>
        <ac:structured-macro ac:name="blog-posts">
          <ac:parameter ac:name="content">excerpts</ac:parameter>
          <ac:parameter ac:name="max">3</ac:parameter>
        </ac:structured-macro>
      </p>
      <p> </p>
    </ac:layout-cell>
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.swdev.volumes"/></ac:parameter>
        <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
        <ac:parameter ac:name="titleColor">white</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="id">volumes-swdev</ac:parameter>
            <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.vision-statement.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.architecture-vision.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.swao.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.swad.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.performance-management.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.security-management.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.test-management.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki"><ac:parameter ac:name="page">projectdoc.volume.usability-management.title</ac:parameter><ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter></ac:structured-macro></li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">analysis-swdev</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#F0F8FF</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.process.analysis"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.vision-statements.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.features.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.out-items.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.user-characters.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.requirements.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.project-constraints.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.quality-targets.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.quality-scenarios.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">design-swdev</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#F0F8FF</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.process.design"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.components.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.views.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.use-cases.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.architecture-aspects.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.architecture-decisions.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
                <li>
                  <p>
                    <ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.technical-debts.home.title</ac:parameter>
                    </ac:structured-macro>
                  </p>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">implementation-swdev</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#F0F8FF</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.process.implementation"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li><at:i18n at:key="projectdoc.content.process.implementation.api-documentation"/></li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.codes.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.properties.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li><at:i18n at:key="projectdoc.content.process.implementation.reports"/></li>
                <li><at:i18n at:key="projectdoc.content.process.implementation.releases"/></li>
                <li><at:i18n at:key="projectdoc.content.process.implementation.changes"/></li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">test-swdev</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#F0F8FF</ac:parameter>
            <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.process.test"/></ac:parameter>
            <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
            <ac:parameter ac:name="titleColor">white</ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li><at:i18n at:key="projectdoc.content.process.test.unit-tests"/></li>
                <li><at:i18n at:key="projectdoc.content.process.test.integration-tests"/></li>
                <li><at:i18n at:key="projectdoc.content.process.test.performance-tests"/></li>
                <li><at:i18n at:key="projectdoc.content.process.test.security-tests"/></li>
                <li><at:i18n at:key="projectdoc.content.process.test.acceptance-tests"/></li>
                <li><at:i18n at:key="projectdoc.content.process.test.usablity-tests"/></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.test-charters.home.title</ac:parameter>
                      <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.content.process.test.exploratory-tests"/></ac:parameter>
                    </ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.test-cases.home.title</ac:parameter>
                      <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.content.process.test.test-cases"/></ac:parameter>
                    </ac:structured-macro></li>
                <li><ac:structured-macro ac:name="projectdoc-link-wiki">
                      <ac:parameter ac:name="page">projectdoc.content.test-datas.home.title</ac:parameter>
                      <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.content.process.test.test-data"/></ac:parameter>
                    </ac:structured-macro></li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="column">
        <ac:parameter ac:name="width">20%</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="id">swdev-deployment</ac:parameter>
            <ac:parameter ac:name="css">projectdoc-homepage-panel-swdev</ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="panel">
                <ac:parameter ac:name="bgColor">#F0F8FF</ac:parameter>
                <ac:parameter ac:name="titleBGColor">#342D7E</ac:parameter>
                <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.process.deployment"/></ac:parameter>
                <ac:parameter ac:name="borderColor">#342D7E</ac:parameter>
                <ac:parameter ac:name="titleColor">white</ac:parameter>
                <ac:rich-text-body>
                  <ul>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.artifacts.home.title</ac:parameter>
                      </ac:structured-macro>
                    </li>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.environments.home.title</ac:parameter>
                      </ac:structured-macro>
                    </li>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.nodes.home.title</ac:parameter>
                      </ac:structured-macro>
                    </li>
                  </ul>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
