/*
 * Copyright 2013-2016 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-alternative',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });       
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-aspect',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-decision',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-component',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-code',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-property',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-feature',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-out-item',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality-scenario',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });


Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality-target',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-requirement',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-use-case',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });


Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-user-character',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-view',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-vision-statement',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-artifact',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-artifact-type',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence:create-doctype-template-environment',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence:create-doctype-template-environment-type',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-node',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-node-type',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-case',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-charter',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-data',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for the document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-report',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-session',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var name = state.pageData["projectdoc.doctype.common.name"];
        if (!name) {
            alert('Please provide a name for this document.');
            return false;
        }
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });
  });
